#!/bin/bash
# Build a docker container with pbuilder
# Need to use "--privileged" to mount the chroot environment, hence this scripting

# Build the base
docker build --tag pbuilder_base .

# Create the pbuilder image and commit
docker stop pbuilder_setup
docker rm pbuilder_setup
docker run --name pbuilder_setup --privileged pbuilder_base pbuilder create --override-config --distribution artful
docker commit --change 'CMD ["/bin/bash"]' pbuilder_setup pbuilder-artful
