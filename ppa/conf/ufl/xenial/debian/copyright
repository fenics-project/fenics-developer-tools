Format: http://svn.debian.org/wsvn/dep/web/deps/dep5.mdwn?rev=174
Upstream-Name: UFL
Upstream-Contact: ufl@lists.launchpad.net
 http://fenicsproject.org
Source: http://launchpad.net/ufl

Files: *
Copyright: 2008-2011, Martin Sandve Alnes <martinal@simula.no>
 2004-2011, Anders Logg <logg@simula.no>
License: LGPL-3+

Files: doc/sphinx/scripts/generate_index.py
 scripts/makedoc
 ufl/algorithms/elementtransformations.py
 ufl/feec.py
 demo/FEEC.ufl
 demo/MixedElasticity.ufl
 demo/VectorLaplaceGradCurl.ufl
Copyright: 2007-2011, Marie E. Rognes
License: LGPL-3+

Files: demo/QuadratureElement.ufl
 demo/RestrictedElement.ufl
Copyright: 2008-2009, Kristian B. Oelgaard
License: LGPL-3+

Files: doc/sphinx/scripts/generate_modules.py
Copyright: 2008, Société des arts technologiques (SAT), http://www.sat.qc.ca/
 2010, Thomas Waldmann <tw AT waldmann-edv DOT de>
License: GPL-2+

Files: ufl/algorithms/expand_indices.py
Copyright: 2008-2011, Martin Sandve Alnes
License: LGPL-3+

Files: demo/StokesEquation.ufl
Copyright: 2005-2009, Anders Logg
 2005-2009, Harish Narayanan
License: LGPL-3+

Files: ufl/permutation.py
Copyright: 2008-2011, Anders Logg
 2008-2011, Kent-Andre Mardal
License: LGPL-3+

Files: demo/SubDomains.ufl
Copyright: 2008, Anders Logg
 2008, Kristian B. Oelgaard
License: LGPL-3+

Files: demo/MixedPoisson.ufl
Copyright: 2006-2009, Anders Logg
 2006-2009, Marie Rognes
License: LGPL-3+

Files: demo/PoissonDG.ufl
Copyright: 2006-2007, Kristiand Oelgaard
 2006-2007, Anders Logg
License: LGPL-3+

Files: doc/sphinx/source/_themes/fenics/static/fenics.css_t
 doc/sphinx/source/_themes/fenics/layout.html
Copyright: 2007-2011, the Sphinx team
License: BSD-2-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 3 of the
 License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package. If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the GNU Lesser General Public
 License, version 3 can be found in the file
 '/usr/share/common-licenses/LGPL-3'.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the GNU General Public License,
 version 2 can be found in the file '/usr/share/common-licenses/GPL-2'.
