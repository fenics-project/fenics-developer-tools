#!/bin/bash

function download_package {
    PACKAGE=$1
    RELEASE=$2

    git clone https://chris_richardson@bitbucket.org/fenics-project/$PACKAGE $PACKAGE-$RELEASE
    (cd $PACKAGE-$RELEASE && git checkout release && rm -rf .git*)
    tar cjf $PACKAGE\_$RELEASE.orig.tar.bz2 $PACKAGE-$RELEASE
}

function build_package {
    PACKAGE=$1
    RELEASE=$2

    cp -au $HOME/fenics-developer-tools/ppa/conf/$PACKAGE/xenial/debian $PACKAGE-$RELEASE
    (cd $PACKAGE-$RELEASE && debuild -S -sa -d)
    pbuilder build $PACKAGE\_$RELEASE-1~ppa1~xenial1.dsc
    (cd /var/cache/pbuilder/result && apt-ftparchive packages . > Packages)
}

RELEASE=2017.2.0
WKDIR=`mktemp -d` && cd $WKDIR
pwd
for package in ufl fiat instant dijitso ffc dolfin mshr ; do
    download_package $package $RELEASE
    build_package $package $RELEASE
done
cd - && rm -rf $WKDIR
