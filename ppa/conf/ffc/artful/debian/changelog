ffc (2017.2.0.post0-1~ppa1~artful1) artful; urgency=medium

  [ FEniCS Project Steering Council ]
  * Release 2017.2.0.post0

  [ Drew Parsons ]
  * Depends on minor not release version of FEniCS components
    i.e. 2017.2 not 2017.2.0 or 2017.2.0.post0

 -- FEniCS Project Steering Council <fenics-steering-council@googlegroups.com>  Sat, 10 Feb 2018 06:46:17 +0000

ffc (2017.2.0-1~ppa1~artful1) artful; urgency=medium

  [ FEniCS Project ]
  * Release 2017.2.0

 -- FEniCS Project Steering Council <fenics-steering-council@googlegroups.com>  Mon, 04 Dec 2017 09:17:35 +0000

ffc (2017.1.0-1~ppa2~zesty1) zesty; urgency=medium

  * Sync from Debian.
  * Mangle Maintainer field.

 -- Johannes Ring <johannr@simula.no>  Thu, 06 Jul 2017 14:33:09 +0200

ffc (2017.1.0-1) experimental; urgency=medium

  * New upstream release.
  * d/patches/fix-float-division.patch: Remove patch (no longer needed).

 -- Johannes Ring <johannr@simula.no>  Thu, 06 Jul 2017 13:32:28 +0200

ffc (2016.2.0-4) unstable; urgency=medium

  [ Johannes Ring ]
  * d/patches/ufc-include-path.patch: Add patch to use /usr/include
    as path to ufc header files.

  [ Drew Parsons ]
  * Enable python3-only installations by using alternatives for
    /usr/include/ufc.h (provided by either python-ffc or python3-ffc).
    ufc.h is used by libdolfin-dev.

 -- Johannes Ring <johannr@simula.no>  Fri, 30 Jun 2017 12:05:38 +0200

ffc (2016.2.0-3) unstable; urgency=medium

  * d/patches/fix-float-division.patch: Add patch to fix float division
    of indices in tensorreordering.

 -- Johannes Ring <johannr@simula.no>  Fri, 02 Jun 2017 11:00:02 +0200

ffc (2016.2.0-2) unstable; urgency=medium

  [ Johannes Ring ]
  * Support Python 3: new package python3-ffc.

 -- Drew Parsons <dparsons@debian.org>  Fri, 27 Jan 2017 22:02:21 +0800

ffc (2016.2.0-1) unstable; urgency=medium

  [ Johannes Ring ]
  * New upstream release.
  * d/watch: Check pgp signature.
  * d/control:
    - Remove Christophe Prud'homme from Uploaders (closes: #835006).
    - Add python-setuptools and dh-python to Build-Depends.
    - Remove python-numpy from Build-Depends.
    - Bump minimum required version for python-fiat, python-instant and
      python-ufl to 2016.2.0.
    - Add python-dijitso to Depends for binary package python-ffc.
    - Change to 'Architecture: all' (no Python extensions anymore).
  * Remove patch (fixed in new upstream release).
  * d/rules:
    - Cleanup (CMake and pkgconfig files are no longer shipped
      with ffc).
  * d/python-ffc.links: Create link to ffc man page for ffc-2 script.
  * d/source.lintian-overrides:
    - Remove old lintian override 'ffc source: source-is-missing'.
    - Add lintian override 'ffc source: outdated-autotools-helper-file'
      for some test files that are not used in the Debian packaging.

  [ Drew Parsons ]
  * constrain versioned dependencies to minor version
    e.g. python-ufl (>= 2016.2.0), python-ufl (<< 2016.3~)

 -- Drew Parsons <dparsons@debian.org>  Tue, 13 Dec 2016 13:41:41 +0800

ffc (2016.1.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Remove swig3.0 from Build-Depends.
    - Bump minimum required version for python-fiat, python-instant and
      python-ufl to 1.6.0.
  * debian/patches/fix-ufc-config.patch: Removed (fixed upstream).
  * Add patch for setup.py for not using --root when getting the
    installation prefix.
  * Add lintian override 'ffc source: source-is-missing' for some .js
    files that are not used and repacking would be overkill.

 -- Johannes Ring <johannr@simula.no>  Mon, 27 Jun 2016 09:13:14 +0200

ffc (1.6.0-3) unstable; urgency=medium

  * Team upload.
  * debian/control:
    + Update VCS fields after the move to Git.
  * Standards-Version: 3.9.8

 -- Drew Parsons <dparsons@debian.org>  Fri, 29 Apr 2016 10:20:19 +0800

ffc (1.6.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Johannes Ring <johannr@simula.no>  Fri, 30 Oct 2015 09:57:33 +0100

ffc (1.6.0-1) experimental; urgency=medium

  * New upstream release.
  * debian/control:
    - Bump minimum required version for python-fiat, python-instant and
      python-ufl to 1.6.0.
    - Replace swig with swig3.0 (>= 3.0.3) in Build-Depends.

 -- Johannes Ring <johannr@simula.no>  Tue, 27 Oct 2015 12:07:57 +0100

ffc (1.5.0-3) unstable; urgency=medium

  * Add patch for fixing broken UFCConfig.cmake (closes: #782317).

 -- Johannes Ring <johannr@simula.no>  Fri, 10 Apr 2015 12:57:50 +0200

ffc (1.5.0-2) unstable; urgency=medium

  * debian/rules: Remove bad directives in pkg-config file ufc-1.pc to
    keep lintian happy.

 -- Johannes Ring <johannr@simula.no>  Tue, 13 Jan 2015 09:18:41 +0100

ffc (1.5.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 3.9.6 (no changes needed).
    - Bump X-Python-Version to >= 2.7.
    - Bump minimum required version for python-fiat, python-instant and
      python-ufl to 1.5.0.
    - Add python-numpy and python-six to Depends field.
    - Bump python-all-dev in Build-Depends to >= 2.7.

 -- Johannes Ring <johannr@simula.no>  Mon, 12 Jan 2015 21:08:58 +0100

ffc (1.4.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Add swig in Build-Depends.
    - Remove python-ufc from Depends.
    - Add ufc and python-ufc to Provides, Conflicts and Replaces.
    - Remove python-ferari and python-dolfin from Suggests.
    - Bump minimum required version for python-fiat, python-instant and
      python-ufl to 1.4.0.
  * debian/rules: Add override for auto clean target to remove generated
    cmake and pkg-config files.

 -- Johannes Ring <johannr@simula.no>  Tue, 03 Jun 2014 18:26:02 +0200

ffc (1.3.0-2) unstable; urgency=medium

  * Bump minimum required version for python-ufc to 2.3.0 and
    python-fiat, python-instant and python-ufl to 1.3.0.

 -- Johannes Ring <johannr@simula.no>  Fri, 10 Jan 2014 14:04:17 +0100

ffc (1.3.0-1) unstable; urgency=low

  * New upstream release.
  * debian/watch: Update URL for move to Bitbucket.
  * debian/docs: README -> README.rst and remove TODO.
  * debian/control:
    - Add python-numpy to Build-Depends.
    - Replace python-all with python-all-dev in Build-Depends.
    - Add ${shlibs:Depends} to Depends.
    - Change to Architecture: any.
    - Bump Standards-Version to 3.9.5 (no changes needed).
  * debian/rules: Call dh_numpy in override_dh_python2.

 -- Johannes Ring <johannr@simula.no>  Fri, 10 Jan 2014 13:56:45 +0100

ffc (1.2.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Bump required version for python-ufc, python-fiat, python-instant
      and python-ufl in Depends field.
    - Bump Standards-Version to 3.9.4.
    - Remove DM-Upload-Allowed field.
    - Bump required debhelper version in Build-Depends.
    - Remove cdbs from Build-Depends.
    - Use canonical URIs for Vcs-* fields.
  * debian/compat: Bump to compatibility level 9.
  * debian/rules: Rewrite for debhelper (drop cdbs).

 -- Johannes Ring <johannr@simula.no>  Wed, 26 Jun 2013 14:48:32 +0200

ffc (1.1.0-1) UNRELEASED; urgency=low

  * New upstream release.
  * debian/control: Bump version numbers for python-ufc, python-fiat,
    python-instant and python-ufl in Depends field.
  * debian/watch: Replace http with https in URL.

 -- Johannes Ring <johannr@simula.no>  Thu, 10 Jan 2013	10:35:02 +0100

ffc (1.0.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Bump version numbers for python-ufc, python-fiat,
    python-instant and python-ufl in Depends field.

 -- Johannes Ring <johannr@simula.no>  Wed, 07 Dec 2011 15:45:54 +0100

ffc (1.0-rc1-1) unstable; urgency=low

  * New upstream release.

 -- Johannes Ring <johannr@simula.no>  Tue, 29 Nov 2011 11:38:38 +0100

ffc (1.0-beta2-1) unstable; urgency=low

  * New upstream release. This release includes some performance
    improvements for evaluating basis functions. It also adds support
    for Bessel functions and error functions.
  * debian/control: Bump version numbers for python-ufc, python-fiat,
    python-instant, and python-ufl.

 -- Johannes Ring <johannr@simula.no>  Wed, 26 Oct 2011 17:52:20 +0200

ffc (1.0-beta-1) unstable; urgency=low

  * New upstream release.
  * Remove patch (applied upstream).

 -- Johannes Ring <johannr@simula.no>  Mon, 15 Aug 2011 17:12:10 +0200

ffc (0.9.10-2) unstable; urgency=low

  * Add patch for removing annoying debug output (closes: #627297).
  * debian/control: Bump minimum required version for python-ufc,
    python-instant and python-ufl in Depends.

 -- Johannes Ring <johannr@simula.no>  Thu, 19 May 2011 13:42:30 +0200

ffc (0.9.10-1) unstable; urgency=low

  * New upstream release. This release introduces some new features and
    some behind-the-scenes improvements. Special quadrature schemes for
    simplices have been added, which will lead to improved performance
    when using the quadrature representation for low-order forms.
  * Move from python-central to dh_python2 (closes: #616807).
    - Remove python-central from Build-Depends.
    - Bump minimum required python-all package version to 2.6.6-3~.
    - Remove XB-Python-Version line.
    - Bump minimum required cdbs version to 0.4.90~.
    - Remove DEB_PYTHON_SYSTEM=pycentral from debian/rules.
    - Replace XS-Python-Version with X-Python-Version.
  * debian/rules:
    - Avoid compressing files with .ufl extension.
    - Use DEB_COMPRESS_EXCLUDE_ALL instead of deprecated
      DEB_COMPRESS_EXCLUDE.
    - Include cdbs utils.mk rule for automated copyright checks.
  * Remove old custom cdbs rules and licensecheck script for copyright
    check.
  * Bump Standards-Version to 3.9.2 (no changes needed).
  * debian/copyright:
    - Update for upstream license change to LGPLv3.
    - Switch to DEP-5 format.

 -- Johannes Ring <johannr@simula.no>  Wed, 18 May 2011 11:22:37 +0200

ffc (0.9.9-1) unstable; urgency=low

  * New upstream release.
  * Remove old field Conflicts, Provides, and Replaces from
    debian/control.
  * Update dependencies on python-ufc, python-fiat, python-instant, and
    python-ufl in debian/control.
  * Remove user manual from debian/docs as this was removed in
    upstream tarball. Also remove debian/doc-base.
  * Update Homepage field in debian/control and Maintainer field in
    debian/copyright.

 -- Johannes Ring <johannr@simula.no>  Wed, 23 Feb 2011 23:51:37 +0100

ffc (0.9.4-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version (no changes needed).

 -- Johannes Ring <johannr@simula.no>  Thu, 02 Sep 2010 11:50:35 +0200

ffc (0.9.3-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Minor fix in Vcs fields.
    - Bump Standards-Version to 3.9.0 (no changes needed).
    - Update version for python-ufc, python-fiat, and python-ufl in
      Depends field.
  * Switch to dpkg-source 3.0 (quilt) format.
  * Update debian/copyright and debian/copyright_hints.

 -- Johannes Ring <johannr@simula.no>  Thu, 01 Jul 2010 19:54:32 +0200

ffc (0.9.2-3) unstable; urgency=low

  * debian/control: Add python-dolfin to Suggests (closes: #581897).

 -- Johannes Ring <johannr@simula.no>  Wed, 19 May 2010 13:06:27 +0200

ffc (0.9.2-2) unstable; urgency=low

  * Package moved from pkg-scicomp to Debian Science.
  * debian/control: Update version dependencies in Depends field.

 -- Johannes Ring <johannr@simula.no>  Wed, 28 Apr 2010 10:33:56 +0200

ffc (0.9.2-1) unstable; urgency=low

  * New upstream release.

 -- Johannes Ring <johannr@simula.no>  Wed, 17 Feb 2010 18:05:07 +0100

ffc (0.9.1-1) unstable; urgency=low

  * New upstream release.
  * Update debian/copyright and debian/copyright_hints.

 -- Johannes Ring <johannr@simula.no>  Tue, 16 Feb 2010 11:52:36 +0100

ffc (0.9.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Bump Standards-Version (no changes needed).
  * Update debian/copyright and debian/copyright_hints.

 -- Johannes Ring <johannr@simula.no>  Wed, 03 Feb 2010 20:22:35 +0100

ffc (0.7.1-1) unstable; urgency=low

  * New upstream release. This version adds support for automatic
    selection of degree and shape of coefficient elements when not
    specified.
  * debian/watch: Update download URL.
  * debian/copyright: Add new files and update Maintainer and Source
    fields.
  * Add debian/doc-base for user manual.

 -- Johannes Ring <johannr@simula.no>  Tue, 08 Dec 2009 10:28:10 +0100

ffc (0.7.0-1) unstable; urgency=low

  * Initial release (Closes: #502980)

 -- Johannes Ring <johannr@simula.no>  Fri, 25 Sep 2009 09:41:39 +0200
