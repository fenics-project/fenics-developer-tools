
### Dockerfile for pbuilder

This contains a recipe for making a docker image containing pbuilder.
Because pbuilder uses chroot, you must run the docker image with --privileged

e.g.

docker run --privileged -ti pbuilder2 /bin/bash

Once in the container, you can build .deb packages as follows:

TODO
