#!/bin/bash
# Build a docker container with pbuilder
# Need to use "--privileged" to mount the chroot environment, hence this scripting

# Build the base
docker build --tag pbuilder_base:xenial .

# Create the pbuilder-xenial image and commit
docker stop pbuilder_setup
docker rm pbuilder_setup
docker run --name pbuilder_setup --privileged pbuilder_base:xenial pbuilder create --override-config --distribution xenial
docker commit --change 'CMD ["/bin/bash"]' pbuilder_setup pbuilder-xenial
