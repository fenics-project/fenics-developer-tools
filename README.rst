======================
FEniCS Developer Tools
======================

This repository contains tools that developers use to make releases of
the various FEniCS software components, including the building of
Ubuntu PPA packages.


License
=======

FEniCS Developer Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FEniCS Developer Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FEniCS Developer Tools. If not, see <http://www.gnu.org/licenses/>.

(This license is just for compatibility with other FEniCS packages,
and these small scripts might just as well have been placed in the
public domain.)


Contact
=======

For comments and requests, send an email to the FEniCS mailing list:

  fenics-dev@googlegroups.com


Contributors
============

  Anders Logg <logg@chalmers.se>
  Johannes Ring <johannr@simula.no>
  Martin Sandve Aln�s <martinal@simula.no>
