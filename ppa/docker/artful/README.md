
### Dockerfile for pbuilder

This contains a recipe for making a docker image containing pbuilder.

`sudo bash ./build-script.sh`

Because pbuilder uses chroot, you must run the docker image with --privileged

e.g.

docker run --privileged -ti pbuilder-artful /bin/bash

Once in the container, you can build .deb packages as follows:

1. Import GPG key
2. TODO