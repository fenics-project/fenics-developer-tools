Source: dolfin
Section: math
Priority: extra
Maintainer: FEniCS Project Steering Council <fenics-steering-council@googlegroups.com>
Uploaders: Chris Richardson <chris@bpi.cam.ac.uk>
Build-Depends:
 cmake (>= 2.8.0),
 debhelper (>= 9),
 dh-python,
 doxygen,
 gfortran,
 libboost-chrono-dev,
 libboost-dev,
 libboost-filesystem-dev,
 libboost-iostreams-dev,
 libboost-math-dev,
 libboost-program-options-dev,
 libboost-serialization-dev,
 libboost-thread-dev,
 libboost-timer-dev,
 libeigen3-dev,
 libhdf5-mpi-dev,
 libsuitesparse-dev,
 libscotch-dev,
 libxml2-dev,
 mpi-default-dev,
 petsc-dev (>= 3.6.4.dfsg1-2),
 slepc-dev (>= 3.6.3.dfsg1-6),
 python-petsc4py,
 python-slepc4py,
 python3-petsc4py,
 python3-slepc4py,
 pkg-config,
 python-dev,
 python3-dev,
 python-pkg-resources,
 python3-pkg-resources,
 python-ffc (>= 2017.2.0), python-ffc (<< 2017.3.0),
 python-dijitso (>= 2017.2.0), python-dijitso (<< 2017.3.0),
 python3-ffc (>= 2017.2.0), python3-ffc (<< 2017.3.0),
 python3-dijitso (>= 2017.2.0), python3-dijitso (<< 2017.3.0),
 python-numpy,
 python3-numpy,
 python-ply,
 python3-ply,
 swig3.0 (>= 3.0.3)
Standards-Version: 3.9.8
X-Python-Version: >= 2.7
X-Python3-Version: >= 3.4
Homepage: http://fenicsproject.org
Vcs-Git: https://bitbucket.org/fenics-project/dolfin.git
Vcs-Browser: https://bitbucket.org/fenics-project/dolfin

Package: libdolfin-dev
Section: libdevel
Architecture: any
Depends:
 cmake (>= 2.8.0),
 libboost-chrono-dev,
 libboost-dev,
 libboost-filesystem-dev,
 libboost-iostreams-dev,
 libboost-math-dev,
 libboost-program-options-dev,
 libboost-serialization-dev,
 libboost-thread-dev,
 libboost-timer-dev,
 libdolfin2017.2 (= ${binary:Version}),
 libeigen3-dev,
 libhdf5-mpi-dev,
 libsuitesparse-dev,
 libscotch-dev,
 libxml2-dev,
 pkg-config,
 python-ffc (>= ${fenics:Upstream-Version}), python-ffc (<< ${fenics:Next-Upstream-Version}),
 python3-ffc (>= ${fenics:Upstream-Version}), python3-ffc (<< ${fenics:Next-Upstream-Version}),
 ${petsc:Depends},
 ${python-petsc4py-alt:Depends},
 ${misc:Depends}
Conflicts:
 libdolfin0-dev,
 libdolfin1.0-dev,
 libdolfin1.1-dev,
 libdolfin1.2-dev,
 libdolfin1.3-dev,
 libdolfin1.4-dev
Replaces:
 libdolfin0-dev,
 libdolfin1.0-dev,
 libdolfin1.1-dev,
 libdolfin1.2-dev,
 libdolfin1.3-dev,
 libdolfin1.4-dev
Description: Shared links and header files for DOLFIN
 DOLFIN is the Python and C++ interface of the FEniCS project for the
 automated solution of differential equations, providing a consistent
 PSE (Problem Solving Environment) for solving ordinary and partial
 differential equations. Key features include a simple, consistent and
 intuitive object-oriented API; automatic and efficient evaluation of
 variational forms; automatic and efficient assembly of linear
 systems; and support for general families of finite elements.
 .
 This package contains the development files.

Package: libdolfin2017.2
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: dolfin-doc, libdolfin2017.2-dbg, python-dolfin
Description: Shared libraries for DOLFIN
 DOLFIN is the Python and C++ interface of the FEniCS project for the
 automated solution of differential equations, providing a consistent
 PSE (Problem Solving Environment) for solving ordinary and partial
 differential equations. Key features include a simple, consistent and
 intuitive object-oriented API; automatic and efficient evaluation of
 variational forms; automatic and efficient assembly of linear
 systems; and support for general families of finite elements.
 .
 This package contains the shared libraries.

Package: libdolfin2017.2-dbg
Section: debug
Architecture: any
Depends: libdolfin2017.2 (= ${binary:Version}), ${misc:Depends}
Description: Shared libraries with debugging symbols for DOLFIN
 DOLFIN is the Python and C++ interface of the FEniCS project for the
 automated solution of differential equations, providing a consistent
 PSE (Problem Solving Environment) for solving ordinary and partial
 differential equations. Key features include a simple, consistent and
 intuitive object-oriented API; automatic and efficient evaluation of
 variational forms; automatic and efficient assembly of linear
 systems; and support for general families of finite elements.
 .
 This package contains the shared libraries with debugging symbols.

Package: python-dolfin
Section: python
Architecture: any
Depends:
 libdolfin-dev (= ${binary:Version}),
 libdolfin2017.2 (= ${binary:Version}),
 python-ffc (>= ${fenics:Upstream-Version}), python-ffc (<< ${fenics:Next-Upstream-Version}),
 python-dijitso (>= ${fenics:Upstream-Version}), python-dijitso (<< ${fenics:Next-Upstream-Version}),
 python-instant (>= ${fenics:Upstream-Version}), python-instant (<< ${fenics:Next-Upstream-Version}),
 python-ufl (>= ${fenics:Upstream-Version}), python-ufl (<< ${fenics:Next-Upstream-Version}),
 python-numpy,
 python-matplotlib,
 python-ply,
 python-six,
 python-sympy,
 ${python-petsc4py:Depends},
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends},
 ${swig:Depends}
Suggests: dolfin-doc
Description: Python interface for DOLFIN (Python 2)
 DOLFIN is the Python and C++ interface of the FEniCS project for the
 automated solution of differential equations, providing a consistent
 PSE (Problem Solving Environment) for solving ordinary and partial
 differential equations. Key features include a simple, consistent and
 intuitive object-oriented API; automatic and efficient evaluation of
 variational forms; automatic and efficient assembly of linear
 systems; and support for general families of finite elements.
 .
 This package contains the Python interface for DOLFIN (Python 2).

Package: python3-dolfin
Section: python
Architecture: any
Depends:
 libdolfin-dev (= ${binary:Version}),
 libdolfin2017.2 (= ${binary:Version}),
 python3-ffc (>= ${fenics:Upstream-Version}), python3-ffc (<< ${fenics:Next-Upstream-Version}),
 python3-dijitso (>= ${fenics:Upstream-Version}), python3-dijitso (<< ${fenics:Next-Upstream-Version}),
 python3-instant (>= ${fenics:Upstream-Version}), python3-instant (<< ${fenics:Next-Upstream-Version}),
 python3-ufl (>= ${fenics:Upstream-Version}), python3-ufl (<< ${fenics:Next-Upstream-Version}),
 python3-numpy,
 python3-matplotlib,
 python3-ply,
 python3-six,
 python3-sympy,
 ${python3-petsc4py:Depends},
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
 ${swig:Depends}
Suggests: dolfin-doc
Description: Python interface for DOLFIN (Python 3)
 DOLFIN is the Python and C++ interface of the FEniCS project for the
 automated solution of differential equations, providing a consistent
 PSE (Problem Solving Environment) for solving ordinary and partial
 differential equations. Key features include a simple, consistent and
 intuitive object-oriented API; automatic and efficient evaluation of
 variational forms; automatic and efficient assembly of linear
 systems; and support for general families of finite elements.
 .
 This package contains the Python interface for DOLFIN (Python 3).

Package: python-dolfin-dbg
Section: debug
Architecture: any
Depends:
 python-dolfin (= ${binary:Version}),
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends}
Suggests: dolfin-doc
Description: Python 2 extension modules for DOLFIN with debugging symbols
 DOLFIN is the Python and C++ interface of the FEniCS project for the
 automated solution of differential equations, providing a consistent
 PSE (Problem Solving Environment) for solving ordinary and partial
 differential equations. Key features include a simple, consistent and
 intuitive object-oriented API; automatic and efficient evaluation of
 variational forms; automatic and efficient assembly of linear
 systems; and support for general families of finite elements.
 .
 This package contains the Python 2 extensions modules for DOLFIN with
 debugging symbols.

Package: python3-dolfin-dbg
Section: debug
Architecture: any
Depends:
 python3-dolfin (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends}
Suggests: dolfin-doc
Description: Python 3 extension modules for DOLFIN with debugging symbols
 DOLFIN is the Python and C++ interface of the FEniCS project for the
 automated solution of differential equations, providing a consistent
 PSE (Problem Solving Environment) for solving ordinary and partial
 differential equations. Key features include a simple, consistent and
 intuitive object-oriented API; automatic and efficient evaluation of
 variational forms; automatic and efficient assembly of linear
 systems; and support for general families of finite elements.
 .
 This package contains the Python 3 extensions modules for DOLFIN with
 debugging symbols.

Package: dolfin-doc
Architecture: all
Section: doc
Depends:
 libdolfin-dev (>= ${source:Version}),
 python-dolfin (>= ${source:Version}),
 ${misc:Depends}
Suggests: cmake (>= 2.8.0)
Description: Documentation and demo programs for DOLFIN
 DOLFIN is the Python and C++ interface of the FEniCS project for the
 automated solution of differential equations, providing a consistent
 PSE (Problem Solving Environment) for solving ordinary and partial
 differential equations. Key features include a simple, consistent and
 intuitive object-oriented API; automatic and efficient evaluation of
 variational forms; automatic and efficient assembly of linear
 systems; and support for general families of finite elements.
 .
 This package contains documentation and demo programs for DOLFIN.

Package: dolfin-bin
Architecture: all
Depends:
 python-dolfin (>= ${source:Version}),
 ${misc:Depends},
 ${python:Depends}
Description: Executable scripts for DOLFIN
 DOLFIN is the Python and C++ interface of the FEniCS project for the
 automated solution of differential equations, providing a consistent
 PSE (Problem Solving Environment) for solving ordinary and partial
 differential equations. Key features include a simple, consistent and
 intuitive object-oriented API; automatic and efficient evaluation of
 variational forms; automatic and efficient assembly of linear
 systems; and support for general families of finite elements.
 .
 This package contains executable scripts for DOLFIN.
