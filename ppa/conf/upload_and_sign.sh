#!/bin/bash

# Login UserID for bitbucket (for uploading)
USER=chris_richardson

# Project name (fiat, ufl, ffc, dolfin, instant, dijitso etc)
PROJECT=$1
VERSION=2017.2.0

echo "Uploading and signing .tar.gz for " $PROJECT
cd $PROJECT

PKGVERSION=$PROJECT-$VERSION

# Remove any old stuff
rm -rf $PKGVERSION $PKGVERSION.tar.gz $PKGVERSION.tar.gz.asc

# Clone archive and checkout the Release branch
git clone https://bitbucket.org/fenics-project/$PROJECT $PKGVERSION
cd $PKGVERSION
git checkout release
rm -rf .git* && cd ..

# Tar and sign the package
tar czf $PKGVERSION.tar.gz $PKGVERSION
rm -rf $PKGVERSION
gpg2 -u DD22BAB3 --armor --detach-sign $PKGVERSION.tar.gz

# Upload to bitbucket downloads
curl --insecure --progress-bar --user $USER --location --fail --form files=@\"$PKGVERSION.tar.gz\" \
     "https://api.bitbucket.org/2.0/repositories/fenics-project/$PROJECT/downloads"

curl --insecure --progress-bar --user $USER --location --fail --form files=@\"$PKGVERSION.tar.gz.asc\" \
     "https://api.bitbucket.org/2.0/repositories/fenics-project/$PROJECT/downloads"

# Clean up
rm -rf $PKGVERSION.tar.gz $PKGVERSION.tar.gz.asc

cd ..
