#!/usr/bin/make -f

#export DH_VERBOSE=1
export PYBUILD_NAME=ffc
export PYBUILD_DISABLE=test
#export PYBUILD_TEST_ARGS={dir}/test
#export PYBUILD_TEST_PYTEST=1

DEB_SOURCE_PACKAGE := $(strip $(shell egrep '^Source: ' debian/control | cut -f 2 -d ':'))
DEB_VERSION := $(shell dpkg-parsechangelog | egrep '^Version:' | cut -f 2 -d ' ')
DEB_NOEPOCH_VERSION := $(shell echo $(DEB_VERSION) | cut -d: -f2-)
DEB_UPSTREAM_VERSION := $(shell echo $(DEB_NOEPOCH_VERSION) | sed 's/-[^-]*$$//')
DEB_STRIPPED_UPSTREAM_VERSION = $(shell echo $(DEB_UPSTREAM_VERSION) | sed -e 's/\+dfsg.*$$//p')

FENICS_RELEASE_VERSION=$(DEB_STRIPPED_UPSTREAM_VERSION)
FENICS_MAJOR_VERSION=$(shell echo $(FENICS_RELEASE_VERSION) | sed "s/^\([^.]*\)\..*$$/\1/")
FENICS_MINOR_VERSION=$(shell echo $(FENICS_RELEASE_VERSION) | sed "s/^\([^.]*\)\.\([^.]*\)\..*$$/\2/")
FENICS_VERSION=$(FENICS_MAJOR_VERSION).$(FENICS_MINOR_VERSION)
FENICS_NEXT_VERSION=$(FENICS_MAJOR_VERSION).$(shell echo $$(( $(FENICS_MINOR_VERSION) + 1 )) )

USCAN_DESTDIR := $(CURDIR)

PYTHON_BASE_SCRIPT="import sys; base=list(filter( lambda p: p.startswith('/usr/lib/python') and p.endswith('/dist-packages'), sys.path ))[0]; sys.stdout.write(base)"
PYTHON_MODULE_BASE := $(shell python -c $(PYTHON_BASE_SCRIPT))
PYTHON3_MODULE_BASE := $(shell python3 -c $(PYTHON_BASE_SCRIPT))
UFC_SUBDIR=ffc/backends/ufc
PYTHON_UFC_DIR=$(PYTHON_MODULE_BASE)/$(UFC_SUBDIR)
PYTHON3_UFC_DIR=$(PYTHON3_MODULE_BASE)/$(UFC_SUBDIR)


%:
	dh $@ --with python2,python3 --buildsystem=pybuild

override_dh_compress:
	dh_compress -X.py -X.ufl

override_dh_clean:
	dh_clean ffc/git_commit_hash.py
	rm -rf *.egg-info

override_dh_auto_install:
	dh_auto_install
# Note: The ffc script is removed for not to conflict with the same script
#       in the python-ffc package
	rm debian/python3-ffc/usr/bin/ffc
	mv debian/python3-ffc/usr/share/man/man1/ffc.1.gz debian/python3-ffc/usr/share/man/man1/ffc-3.1.gz

override_dh_installdeb:
	dh_installdeb
	# handle alternatives for ufc.h
	for pkg in $$(dh_listpackages -i); do \
	  sed -i -e 's|__PYTHON_UFC_DIR__|$(PYTHON_UFC_DIR)|; s|__PYTHON3_UFC_DIR__|$(PYTHON3_UFC_DIR)|' debian/$$pkg/DEBIAN/*; \
	done

override_dh_gencontrol:
	dh_gencontrol -- -Vfenics:Upstream-Version=$(FENICS_VERSION) -Vfenics:Next-Upstream-Version=$(FENICS_NEXT_VERSION)~

.PHONY: get-orig-source
get-orig-source:
	uscan --force-download --verbose --destdir $(USCAN_DESTDIR)
