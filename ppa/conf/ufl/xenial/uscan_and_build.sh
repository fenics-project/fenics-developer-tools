#!/bin/bash

PROJECT=ufl
VERSION=2017.2.0
PKGVERSION=$PROJECT-$VERSION

uscan --force-download --destdir .
tar xf $PKGVERSION.tar.gz
cp -au debian $PKGVERSION && cd $PKGVERSION
debuild -S -sa -d

# dput fenics-exp-ppa ufl_2017.2.0-1~ppa1~xenial1_source.changes
