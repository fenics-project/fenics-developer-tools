
Getting set up
==============

You need to have a GPG key available locally. Use the one for the Steering Committee.

gpg --import public_key.gpg
gpg --allow-secret-key-import --import private_key.gpg

Check with "gpg -k". The key DD22BAB3 should be listed.

It should be registered with Ubuntu already, but if you need to register another key:

gpg --send-keys --keyserver keyserver.ubuntu.com <KEY-ID>

Stage I
-------

There is a script in ppa/conf which will download a package from Bitbucket, create a .tar.gz of a specified branch,
sign it with the key, and push back to Bitbucket Downloads. This is a prerequisite step for building.
Make sure there is a branch on Bitbucket named with the release ID (e.g. 2018.1.0) and that all "ChangeLog.rst", "setup.py" have been updated.

Run like this:

./upload_and_sign.sh ufl

etc. for each package. It may be necessary to edit the script (e.g. change Bitbucket userid).

After this stage, there should be .tar.gz and .tar.gz.asc for each package on Bitbucket Downloads for each respective project

Stage II
--------

To build for a specific distribution, the "debian/changelog" file needs to be updated. Go to the level above the "debian" folder, and run "dch". Change the entry to look like this: pay attention to the email address (must be exact) and the release number in brackets ().

------------------------------------------------------------------------------------------------------------------
ufl (2017.2.0-1~ppa1~xenial1) xenial; urgency=medium

  [ FEniCS Project ]
  * Release of 2017.2.0

 --  FEniCS Project Steering Council <fenics-steering-council@googlegroups.com>  Thu, 14 Sep 2017 13:52:48 +0100
------------------------------------------------------------------------------------------------------------------

After correctly editing the "debian/changelog", run the "uscan_and_build.sh" script, which should pull the .tar.gz and create a file called foo.xxx.x.x.changes

Stage III
---------
Upload the "changes" file to launchpad using dput (see config below).

dput fenics-exp-ppa xxxxxxxxxx.changes

This stage will cause launchpad to build the .deb packages. It is somewhat "final", since you cannot delete the .tar.gz once uploaded to launchpad.




The following lines should be added in ~/.dput.cf:

  [fenics-ppa]
  fqdn = ppa.launchpad.net
  method = ftp
  incoming = ~fenics-packages/fenics/ubuntu/
  login = anonymous
  allow_unsigned_uploads = 0
  allowed_distributions = .*

  [fenics-exp-ppa]
  fqdn = ppa.launchpad.net
  method = ftp
  incoming = ~fenics-packages/fenics-exp/ubuntu/
  login = anonymous
  allow_unsigned_uploads = 0
  allowed_distributions = .*
