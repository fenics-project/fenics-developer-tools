Source: mshr
Section: math
Priority: optional
Maintainer: FEniCS Project Steering Council <fenics-steering-council@googlegroups.com>
Uploaders: Chris Richardson <chris@bpi.cam.ac.uk>
Build-Depends: debhelper (>= 9),
 python-dev,
 dh-python,
 cmake (>= 2.8.0),
 swig3.0 (>= 3.0.3),
 libmpfr-dev,
 libgmp-dev,
 libdolfin-dev (>= 2017.2.0),
 libpython3.6-dev,
 python-ffc (>= 2017.2.0),
 python-numpy,
 python-slepc4py,
 python3-slepc4py,
 python-petsc4py,
 python3-petsc4py,
 libproj-dev
Standards-Version: 3.9.8
X-Python-Version: >= 2.5
Homepage: http://fenicsproject.org
Vcs-Git: https://bitbucket.org/fenics-project/mshr.git
Vcs-Browser: https://bitbucket.org/fenics-project/mshr

Package: libmshr2017.2
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Shared libraries for mshr
 mshr generates simplicial DOLFIN meshes in 2D and 3D from geometries
 described by Constructive Solid Geometry (CSG) or from surface files,
 utilizing CGAL and Tetgen as mesh generation backends.
 .
 This package contains the shared libraries.

Package: libmshr-dev
Section: libdevel
Architecture: any
Depends: libmshr2017.2 (= ${binary:Version}),
 libdolfin-dev (>= 2017.2.0),
 cmake (>= 2.8.0),
 libproj-dev,
 ${shlibs:Depends},
 ${misc:Depends}
Description: Shared links and header files for mshr
 mshr generates simplicial DOLFIN meshes in 2D and 3D from geometries
 described by Constructive Solid Geometry (CSG) or from surface files,
 utilizing CGAL and Tetgen as mesh generation backends.
 .
 This package contains the development files.

Package: python-mshr
Section: python
Architecture: any
Depends: libmshr-dev (= ${binary:Version}),
 libmshr2017.2 (= ${binary:Version}),
 python-dolfin (>= 2017.2.0),
 dolfin-bin (>= 2017.2.0),
 ${swig:Depends},
 ${python:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Description: Python interface for mshr
 mshr generates simplicial DOLFIN meshes in 2D and 3D from geometries
 described by Constructive Solid Geometry (CSG) or from surface files,
 utilizing CGAL and Tetgen as mesh generation backends.
 .
 This package contains the Python interface for DOLFIN.

Package: python3-mshr
Section: python
Architecture: any
Depends: libmshr-dev (= ${binary:Version}),
 libmshr2017.2 (= ${binary:Version}),
 python3-dolfin (>= 2017.2.0),
 ${swig:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Description: Python3 interface for mshr
 mshr generates simplicial DOLFIN meshes in 2D and 3D from geometries
 described by Constructive Solid Geometry (CSG) or from surface files,
 utilizing CGAL and Tetgen as mesh generation backends.
 .
 This package contains the Python interface for DOLFIN.

Package: mshr-demos
Section: doc
Architecture: all
Depends: libmshr-dev (>= ${source:Version}),
 python-mshr (>= ${source:Version}),
 libdolfin-dev (>= 2017.2.0),
 python-dolfin (>= 2017.2.0),
 ${shlibs:Depends},
 ${misc:Depends}
Description: Examples and demo programs for mshr
 mshr generates simplicial DOLFIN meshes in 2D and 3D from geometries
 described by Constructive Solid Geometry (CSG) or from surface files,
 utilizing CGAL and Tetgen as mesh generation backends.
 .
 This package contains examples and demo programs.
