#!/bin/bash
PACKAGE=$1
echo $PACKAGE

git clone https://chris_richardson@bitbucket.org/fenics-project/$PACKAGE $PACKAGE-2017.2.0

cd $PACKAGE-2017.2.0 && git checkout release
rm -rf .git*
cd ..
tar cjf $PACKAGE\_2017.2.0.orig.tar.bz2 $PACKAGE-2017.2.0

cd $PACKAGE-2017.2.0
cp -au /root/fenics-developer-tools/ppa/conf/$PACKAGE/artful/debian .
debuild -S -sa -d
cd ..

pbuilder build $PACKAGE\_2017.2.0-1~ppa1~artful1.dsc
(cd /var/cache/pbuilder/result && apt-ftparchive packages . > Packages)
